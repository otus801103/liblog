#include "logger.h"
#include <stdarg.h>
#include <string.h>
#include <execinfo.h> // для backtrace и backtrace_symbols
#include <unistd.h>

#define MAX_BUFFER_SIZE 1024

static FILE *log_file = NULL;
static const char *log_level_names[] = {"DEBUG", "INFO", "WARNING", "ERROR"};

void set_log_file(const char *filename) {
    if (log_file) {
        fclose(log_file);
    }
    log_file = fopen(filename, "a"); // Открыть файл для добавления сообщений
    if (!log_file) {
        perror("Error opening log file");
        exit(EXIT_FAILURE);
    }
    setbuf(log_file, NULL); // Отключить буферизацию
}

static void v_log_message(LogLevel level, const char *file, int line, const char *format, va_list args) {
    if (!log_file) return;
    char buffer[MAX_BUFFER_SIZE];
    int offset = snprintf(buffer, sizeof(buffer), "[%s] %s:%d: ", log_level_names[level], file, line);
    vsnprintf(buffer + offset, sizeof(buffer) - offset, format, args);

    fprintf(log_file, "%s\n", buffer);
    fflush(log_file); // Сбросить буфер файла, чтобы сразу записать сообщение
}

void logmessage(LogLevel level, const char *file, int line, const char *format, ...) {
    va_list args;
    va_start(args, format);
    v_log_message(level, file, line, format, args);
    va_end(args);
}

void logerror(const char *file, int line, const char *format, ...) {
    va_list args;
    va_start(args, format);
    v_log_message(LOGERROR, file, line, format, args);
    va_end(args);

    // Отобразить стек вызовов при ошибке
    void *array[10];
    size_t size;
    char **strings;
    size = backtrace(array, 10);
    strings = backtrace_symbols(array, size);
    fprintf(log_file, "Error stacktrace:\n");
    for (size_t i = 0; i < size; i++) {
        fprintf(log_file, "%s\n", strings[i]);
    }
    free(strings);
}

__attribute__((constructor)) static void logger_init() {
    set_log_file("default_log.txt");
}

__attribute__((destructor)) static void logger_cleanup() {
    if (log_file) {
        fclose(log_file);
        log_file = NULL;
    }
}


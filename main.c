#include "logger.h"

int main() {

    LOGDEBUG("This is a debug message. Code: %d",001);
    LOGINFO("Program started successfully. Code: %d",002);
    LOGWARNING("This is a warning message. Code: %d",003);
    LOGERROR("This is an error message. Code: %d",004);

    return 0;
}
#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>
#include <stdlib.h>

typedef enum {
    LOGDEBUG, LOGINFO, LOGWARNING, LOGERROR
} LogLevel;

void logmessage(LogLevel level, const char *file, int line, const char *format, ...);
void logerror(const char *file, int line, const char *format, ...);
void set_log_file(const char *filename);


// Для использования в коде при вызове:

#define LOGDEBUG(format, ...) logmessage(LOGDEBUG, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define LOGINFO(format, ...) logmessage(LOGINFO, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define LOGWARNING(format, ...) logmessage(LOGWARNING, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define LOGERROR(format, ...) logerror(__FILE__, __LINE__, format, ##__VA_ARGS__)

#endif

